import {gCars,checkNew} from "./info"
function App() {
  return (
    <div>
      <ul>
        {gCars.map((element,index) =>{
          return <li key={index}>{element.make + " "+ element.model + " : "+ element.vID+ " "+checkNew(element)}</li>
        }) }
      </ul>
    </div>
  );
}

export default App;
